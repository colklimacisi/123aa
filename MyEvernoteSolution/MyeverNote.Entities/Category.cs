﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyeverNote.Entities
{
  [Table("Categories")]
  public  class Category:MyEntitiyBase
    {
        [Required,StringLength(50)]
        public string Title { get; set; }
        [StringLength(200)]
        public string Description { get; set; }

        public virtual List<Note> Notes { get; set; } //Bir kategorının bırden cok notu olabilri..
    }
}
